﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacters.DTOs.CharacterDtos;
using MovieCharacters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacters.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharacterController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharacterController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all the characters currently in the database
        /// </summary>
        /// <returns>JSON object containing all character JSON objects</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            var characters = await _context.character.ToListAsync();
            var readCharacters = _mapper.Map<List<CharacterReadDTO>>(characters);
            return Ok(readCharacters);

        }
        /// <summary>
        /// Get a character with a specific ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Json object containing the relevant character</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {
            var character = await _context.character.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }
            var characterReadDto = _mapper.Map<CharacterReadDTO>(character);
            return Ok(characterReadDto);
        }

        /// <summary>
        /// Post a new character to database
        /// Should include charactername, gender and a link to img of character.
        /// Can also include character alias.
        /// </summary>
        /// <param name="characterCreateDto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Character>> PostCharacter([FromBody] CharacterCreateDTO characterCreateDto)
        {
            var character = _mapper.Map<Character>(characterCreateDto);
            try
            {
                _context.character.Add(character);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            var newCharacter = _mapper.Map<CharacterReadDTO>(character);
            return CreatedAtAction("GetCharacterById", new { id = newCharacter.characterId }, newCharacter);
        }

        /// <summary>
        /// Change the values of a character in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterEditDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Character>> UpdateCharacter(int id, [FromBody] CharacterEditDTO characterEditDto)
        {
            if (id != characterEditDto.characterId)
            {
                return BadRequest();
            }
            try
            {
                var character = _mapper.Map<Character>(characterEditDto);
                _context.Entry(character).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            return NoContent();
        }

        /// <summary>
        /// Delete a character from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteCharacter(int id) 
        {
            var character = await _context.character.FindAsync(id);
            if(character == null)
            {
                return NotFound();
            }
            try
            {
                _context.character.Remove(character);
                await _context.SaveChangesAsync();
            }
            catch 
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            return NoContent();
        }
    }
}
