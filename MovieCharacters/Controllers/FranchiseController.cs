﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacters.DTOs.CharacterDtos;
using MovieCharacters.DTOs.FranchiseDtos;
using MovieCharacters.DTOs.MovieDtos;
using MovieCharacters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacters.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]

    public class FranchiseController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public FranchiseController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all the (movie) Franchises from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchises = await _context.franchise.ToListAsync();
            var readFranchises = _mapper.Map<List<FranchiseReadDTO>>(franchises);
            return Ok(readFranchises);
        }
        /// <summary>
        /// Get a specific franchise with id as specified
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            var franchise = await _context.franchise.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            var franchiseReadDto = _mapper.Map<FranchiseReadDTO>(franchise);
            return Ok(franchiseReadDto);
        }
        /// <summary>
        /// Post a new franchise to the database
        /// include name and description
        /// </summary>
        /// <param name="franchiseCreateDto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Franchise>> PostFranchise([FromBody] FranchiseCreateDTO franchiseCreateDto)
        {
            var franchise = _mapper.Map<Franchise>(franchiseCreateDto);
            try
            {
                _context.franchise.Add(franchise);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            var newFranchise = _mapper.Map<FranchiseReadDTO>(franchise);
            return CreatedAtAction("GetFranchiseById", new { id = newFranchise.franchiseId }, newFranchise);
        }
        /// <summary>
        /// Change the value of the name and/or description of a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseEditDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Franchise>> UpdateFranchise(int id, [FromBody] FranchiseEditDTO franchiseEditDto)
        {
            if (id != franchiseEditDto.franchiseId)
            {
                return BadRequest();
            }
            try
            {
                var franchise = _mapper.Map<Franchise>(franchiseEditDto);
                _context.Entry(franchise).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            return NoContent();
        }

        /// <summary>
        /// Remove a franchise, by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.franchise.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            try
            {
                _context.franchise.Remove(franchise);
                await _context.SaveChangesAsync();
            }
            catch 
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            return NoContent();
        }

        /// <summary>
        /// Gets all movies in a given franchise. Select franchise by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("movies/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMoviesInFranchise(int id)
        {
            var franchise = await _context.franchise.Include(f => f.movies).FirstOrDefaultAsync(f => f.franchiseId == id);
            if (franchise == null)
            {
                return NotFound();
            }
            var movies = franchise.movies;
            var movieDTO = _mapper.Map<List<MovieReadDTO>>(movies);
            return Ok(movieDTO);
        }

        /// <summary>
        /// Get all characters in a franchises movie, select franchise by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("characters/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInFranchise(int id)
        {
            var franchise = await _context.franchise.Include(f => f.movies).ThenInclude(m => m.characters).FirstOrDefaultAsync(f => f.franchiseId == id);
            if (franchise == null)
            {
                return NotFound();
            }
            HashSet<CharacterReadDTO> characters = new();
            List<int> characterIds = new();
            foreach (Movie movie in franchise.movies)
            {
                foreach(Character character in movie.characters) {
                    if (characterIds.Contains(character.characterId))
                    {
                        continue;
                    }
                    characterIds.Add(character.characterId);
                    var characterReadDto = _mapper.Map<CharacterReadDTO>(character);
                    characters.Add(characterReadDto);
                }
            }
            return Ok(characters);
        }

        /// <summary>
        /// Method that posts existing movies to an existing franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]

        public async Task<ActionResult> AssignMoviesToFranchise(int id, [FromBody] List<int> movies)
        {
            var franchise = await _context.franchise.Include(f => f.movies).FirstOrDefaultAsync(f => f.franchiseId == id);
            if (franchise == null)
            {
                return NotFound();
            }
            foreach (var movieId in movies)
            {
                var tempMovie = await _context.movie.FirstOrDefaultAsync(m => m.movieId == movieId);
                if (tempMovie != null)
                {
                    franchise.movies.Add(tempMovie);
                }
            }
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}
