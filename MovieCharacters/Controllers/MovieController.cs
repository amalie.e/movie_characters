﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacters.DTOs.CharacterDtos;
using MovieCharacters.DTOs.MovieDtos;
using MovieCharacters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacters.Controllers
{
    [Route("api/Movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MovieController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public MovieController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all movies 
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies()
        {
            var movies = await _context.movie.ToListAsync();
            var readMovies = _mapper.Map<List<MovieReadDTO>>(movies);

            return Ok(readMovies);
        }
        /// <summary>
        /// Gets one movie by Id
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>

        [HttpGet("{movieId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MovieReadDTO>> GetById(int movieId)
        {
            var movie = await _context.movie.FindAsync(movieId);

            if (movie == null)
            {
                return NotFound();
            }
            var movieReadDto = _mapper.Map<MovieReadDTO>(movie);
            return Ok(movieReadDto);
        }


        /// <summary>
        /// Posts one new movie
        /// </summary>
        /// <param name="movieCreateDto"></param>
        /// <returns></returns>

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Movie>> PostMovie([FromBody] MovieCreateDTO movieCreateDto)
        {
            var movie = _mapper.Map<Movie>(movieCreateDto);
            try
            {
                _context.movie.Add(movie);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            var newMovie = _mapper.Map<MovieReadDTO>(movie);
            return CreatedAtAction("GetById", new { movieId = newMovie.movieId }, newMovie);
        }

        /// <summary>
        /// Deletes one movie by Id
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        [HttpDelete("{movieId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> DeleteMovie(int movieId)
        {
            var movie = await _context.movie.FindAsync(movieId);
            if (movie == null)
            {
                return NotFound();
            }
            _context.movie.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();

        }
        /// <summary>
        /// Changes a value of a movie by Id
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="movieEditDto"></param>
        /// <returns></returns>
        [HttpPut("{movieId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]

        public async Task<ActionResult> UpdateMovie(int movieId, [FromBody] MovieEditDTO movieEditDto)
        {
            if (movieId != movieEditDto.movieId)
            {
                return BadRequest();
            }
            var movie = _mapper.Map<Movie>(movieEditDto);
            _context.Entry(movie).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Gets all characters in a movie, select movie by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("characters/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInMovie(int id)
        {
            var movie = await _context.movie.Include(m => m.characters).FirstOrDefaultAsync(m => m.movieId == id);
            if (movie == null)
            {
                return NotFound();
            }
            HashSet<CharacterReadDTO> characters = new();
            foreach (Character character in movie.characters)
            {
                var characterReadDto = _mapper.Map<CharacterReadDTO>(character);
                characters.Add(characterReadDto);
            }

            return Ok(characters);

        }

        /// <summary>
        /// Method to post existing characters to an existing movie.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns></returns>
        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> AssignCharactersToMovie(int id, [FromBody] List<int> characters)
        {
            var movie = await _context.movie.Include(m => m.characters).FirstOrDefaultAsync(m => m.movieId == id);
            if (movie == null)
            {
                return NotFound();
            }
            foreach (var charId in characters)
            {
                var tempCharacter = await _context.character.FirstOrDefaultAsync(c => c.characterId == charId);
                if (tempCharacter != null)
                {
                    movie.characters.Add(tempCharacter);
                }
            }
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}
