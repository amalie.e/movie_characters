﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.DTOs.CharacterDtos
{
    public class CharacterCreateDTO
    {

        [Required]
        [MaxLength(50)]
        public string characterName { get; set; }

        [MaxLength(50)]
        public string alias { get; set; }

        [Required]
        [MaxLength(6)]
        public string gender { get; set; }

        [Required]
        [MaxLength(100)]
        public string characterPicture { get; set; }

    }
}
