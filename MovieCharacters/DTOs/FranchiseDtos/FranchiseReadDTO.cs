﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.DTOs.FranchiseDtos
{
    public class FranchiseReadDTO
    {
        public int franchiseId { get; set; }

        [Required]
        [MaxLength(25)]
        public string franchiseName { get; set; }

        [Required]
        [MaxLength(140)]
        public string description { get; set; }
    }
}
