﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.DTOs.MovieDtos
{
    public class MovieEditDTO
    {
        public int movieId { get; set; }

        [Required]
        [MaxLength(50)]
        public string movieTitle { get; set; }
        [Required]
        [MaxLength(75)]
        public string genre { get; set; }
        [Required]
        [MaxLength(4)]
        public string releaseYear { get; set; }
        [Required]
        [MaxLength(30)]
        public string director { get; set; }
        [MaxLength(100)]
        public string moviePicture { get; set; }
        [MaxLength(100)]
        public string trailerUrl { get; set; }
    }
}
