﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.DTOs.MovieDtos
{
    public class MovieReadDTO
    {
        public int movieId { get; set; }

    
        [MaxLength(50)]
        public string movieTitle { get; set; }
    
        [MaxLength(75)]
        public string genre { get; set; }

        [MaxLength(4)]
        public string releaseYear { get; set; }
        [MaxLength(30)]
        public string director { get; set; }
    }
}
