﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacters.Migrations
{
    public partial class fi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "character",
                columns: table => new
                {
                    characterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    characterName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    gender = table.Column<string>(type: "nvarchar(6)", maxLength: 6, nullable: false),
                    characterPicture = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_character", x => x.characterId);
                });

            migrationBuilder.CreateTable(
                name: "franchise",
                columns: table => new
                {
                    franchiseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    franchiseName = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    description = table.Column<string>(type: "nvarchar(140)", maxLength: 140, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_franchise", x => x.franchiseId);
                });

            migrationBuilder.CreateTable(
                name: "movie",
                columns: table => new
                {
                    movieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    movieTitle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    genre = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: false),
                    releaseYear = table.Column<string>(type: "nvarchar(4)", maxLength: 4, nullable: false),
                    director = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    moviePicture = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    trailerUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    franchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_movie", x => x.movieId);
                    table.ForeignKey(
                        name: "FK_movie_franchise_franchiseId",
                        column: x => x.franchiseId,
                        principalTable: "franchise",
                        principalColumn: "franchiseId",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Starring",
                columns: table => new
                {
                    movieId = table.Column<int>(type: "int", nullable: false),
                    characterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Starring", x => new { x.movieId, x.characterId });
                    table.ForeignKey(
                        name: "FK_Starring_character_characterId",
                        column: x => x.characterId,
                        principalTable: "character",
                        principalColumn: "characterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Starring_movie_movieId",
                        column: x => x.movieId,
                        principalTable: "movie",
                        principalColumn: "movieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "character",
                columns: new[] { "characterId", "alias", "characterName", "characterPicture", "gender" },
                values: new object[,]
                {
                    { 1, "Mr.Anderson", "Neo", "https://upload.wikimedia.org/wikipedia/en/c/c6/NeoTheMatrix.jpg", "male" },
                    { 2, "Iron Man", "Tony Stark", "https://upload.wikimedia.org/wikipedia/en/f/f2/Robert_Downey_Jr._as_Tony_Stark_in_Avengers_Infinity_War.jpg", "male" },
                    { 3, null, "Pepper Potts", "https://static.wikia.nocookie.net/disney/images/5/56/Pepper_Potts.jpg/revision/latest?cb=20181223155649", "female" },
                    { 4, "Captain America", "Steve Rogers", "https://upload.wikimedia.org/wikipedia/en/6/6b/Chris_Evans_as_Steve_Rogers_Captain_America.jpg", "male" },
                    { 5, "Black Widow", "Natasha Romanoff", "https://upload.wikimedia.org/wikipedia/en/f/f6/Scarlett_Johansson_as_Black_Widow.jpg", "female" }
                });

            migrationBuilder.InsertData(
                table: "franchise",
                columns: new[] { "franchiseId", "description", "franchiseName" },
                values: new object[,]
                {
                    { 1, "Harry Potter is a wizard", "Harry Potter" },
                    { 2, "Superheroes saving the world aka America", "Marvel Cinematic Universe" },
                    { 3, "We're living in a simulation", "Matrix" }
                });

            migrationBuilder.InsertData(
                table: "movie",
                columns: new[] { "movieId", "director", "franchiseId", "genre", "moviePicture", "movieTitle", "releaseYear", "trailerUrl" },
                values: new object[,]
                {
                    { 5, "Chris Columbus", null, "Fantasy, Adventure", "https://m.media-amazon.com/images/M/MV5BMTcxODgwMDkxNV5BMl5BanBnXkFtZTYwMDk2MDg3._V1_FMjpg_UX1000_.jpg", "Harry Potter and the chamber of secrets", "2002", "https://www.youtube.com/watch?v=1bq0qff4iF8" },
                    { 6, "David Yates", null, "Fantasy, Adventure", "https://m.media-amazon.com/images/M/MV5BNzU3NDg4NTAyNV5BMl5BanBnXkFtZTcwOTg2ODg1Mg@@._V1_.jpg", "Harry Potter and the half blood prince", "2009", "https://www.youtube.com/watch?v=JYLdTuL9Wjw" }
                });

            migrationBuilder.InsertData(
                table: "movie",
                columns: new[] { "movieId", "director", "franchiseId", "genre", "moviePicture", "movieTitle", "releaseYear", "trailerUrl" },
                values: new object[,]
                {
                    { 1, "Alfonso Cuarón", 1, "Fantasy, Adventure", "https://m.media-amazon.com/images/M/MV5BMTY4NTIwODg0N15BMl5BanBnXkFtZTcwOTc0MjEzMw@@._V1_FMjpg_UX1000_.jpg", "Harry Potter and the Prisoner of Azkaban", "2004", "https://www.youtube.com/watch?v=lAxgztbYDbs" },
                    { 2, "John Favreau", 2, "Action, Adventure", "https://upload.wikimedia.org/wikipedia/en/0/02/Iron_Man_%282008_film%29_poster.jpg", "Ironman", "2008", "https://www.youtube.com/watch?v=8ugaeA-nMTc" },
                    { 3, "Wachowski sisters", 3, "Science-Fiction, Action", "https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg", "Matrix", "1999", "https://www.youtube.com/watch?v=vKQi3bBA1y8" },
                    { 4, "Wachowski sisters", 3, "Science-Fiction, Action", "https://m.media-amazon.com/images/M/MV5BODE0MzZhZTgtYzkwYi00YmI5LThlZWYtOWRmNWE5ODk0NzMxXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg", "The Matrix Reloaded", "2003", "https://www.youtube.com/watch?v=kYzz0FSgpSU" }
                });

            migrationBuilder.InsertData(
                table: "Starring",
                columns: new[] { "characterId", "movieId" },
                values: new object[,]
                {
                    { 2, 2 },
                    { 3, 2 },
                    { 1, 3 },
                    { 1, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_movie_franchiseId",
                table: "movie",
                column: "franchiseId");

            migrationBuilder.CreateIndex(
                name: "IX_Starring_characterId",
                table: "Starring",
                column: "characterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Starring");

            migrationBuilder.DropTable(
                name: "character");

            migrationBuilder.DropTable(
                name: "movie");

            migrationBuilder.DropTable(
                name: "franchise");
        }
    }
}
