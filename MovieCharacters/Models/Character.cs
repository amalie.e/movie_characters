﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.Models
{
    /// <summary>
    /// A class representing a Character (from a movie)
    /// Contains information about the character
    /// And the relationship to the movie(s) in which the character appears
    /// </summary>
    public class Character
    {
        public int characterId { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string characterName { get; set; }

        [MaxLength(50)]
        public string alias { get; set; }
        
        [Required]
        [MaxLength(6)]
        public string gender { get; set; }
       
        [Required]
        public string characterPicture { get; set; }

        //Relationship
        public ICollection<Movie> movies { get; set; } = new HashSet<Movie>();


    }
}
