﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.Models
{
    /// <summary>
    /// A class representing a Franchise
    /// Contains information about the franchise and the relationship to the movies belonging to a franchise
    /// </summary>
    public class Franchise
    {
        public int franchiseId { get; set; }

        [Required]
        [MaxLength(25)]
        public string franchiseName { get; set; }

        [Required]
        [MaxLength(140)]
        public string description { get; set; }
        //Relationship
        public ICollection<Movie> movies { get; set; }

    }
}
