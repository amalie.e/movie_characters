﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.Models
{
    /// <summary>
    /// A class representing a Movie
    /// Contains different information about the movie and its relationship to characters and franchises
    /// </summary>
    public class Movie
    {
        public int movieId { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string movieTitle { get; set; }
        [Required]
        [MaxLength(75)]
        public string genre { get; set; }
        [Required]
        [MaxLength(4)]
        public string releaseYear { get; set; }
        [Required]
        [MaxLength(30)]
        public string director { get; set;}
        public string moviePicture { get; set; }
        public string trailerUrl { get; set; }
        //Relationship
        public int? franchiseId { get; set; }
        public Franchise franchise { get; set; }
        //Relationship
        public ICollection<Character> characters { get; set; }

    }
}
