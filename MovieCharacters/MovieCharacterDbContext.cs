﻿using Microsoft.EntityFrameworkCore;
using MovieCharacters.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters
{
    public class MovieCharacterDbContext : DbContext
    {
        // Define the different Database sets 
        public DbSet<Character> character { get; set; }
        public DbSet<Movie> movie { get; set; }
        public DbSet<Franchise> franchise { get; set; }


        /// <summary>
        /// Constructor that handles the injection of the sqlServer
        /// </summary>
        /// <param name="options"></param>
        public MovieCharacterDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //Seed Data for Franchise 
            modelBuilder.Entity<Franchise>().HasData(new Franchise()
            {
                franchiseId = 1,
                franchiseName = "Harry Potter",
                description = "Harry Potter is a wizard"
            }); 
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise()
                {
                    franchiseId = 2,
                    franchiseName = "Marvel Cinematic Universe",
                    description = "Superheroes saving the world aka America"
                });
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise()
                {
                    franchiseId = 3,
                    franchiseName = "Matrix",
                    description = "We're living in a simulation"
                });
            //Seed Data for Movie 
            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    movieId = 1,
                    movieTitle = "Harry Potter and the Prisoner of Azkaban",
                    genre = "Fantasy, Adventure",
                    releaseYear = "2004",
                    director = "Alfonso Cuarón",
                    moviePicture = "https://m.media-amazon.com/images/M/MV5BMTY4NTIwODg0N15BMl5BanBnXkFtZTcwOTc0MjEzMw@@._V1_FMjpg_UX1000_.jpg",
                    trailerUrl = "https://www.youtube.com/watch?v=lAxgztbYDbs",
                    franchiseId = 1
                });
            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    movieId = 2,
                    movieTitle = "Ironman",
                    genre = "Action, Adventure",
                    releaseYear = "2008",
                    director = "John Favreau",
                    moviePicture = "https://upload.wikimedia.org/wikipedia/en/0/02/Iron_Man_%282008_film%29_poster.jpg",
                    trailerUrl = "https://www.youtube.com/watch?v=8ugaeA-nMTc",
                    franchiseId = 2
                }); 
            modelBuilder.Entity<Movie>().HasData(
                 new Movie()
                 {
                     movieId = 3,
                     movieTitle = "Matrix",
                     genre = "Science-Fiction, Action",
                     releaseYear = "1999",
                     director = "Wachowski sisters",
                     moviePicture = "https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg",
                     trailerUrl = "https://www.youtube.com/watch?v=vKQi3bBA1y8",
                     franchiseId = 3
                 });
            modelBuilder.Entity<Movie>().HasData(
                 new Movie()
                 {
                     movieId = 4,
                     movieTitle = "The Matrix Reloaded",
                     genre = "Science-Fiction, Action",
                     releaseYear = "2003",
                     director = "Wachowski sisters",
                     moviePicture = "https://m.media-amazon.com/images/M/MV5BODE0MzZhZTgtYzkwYi00YmI5LThlZWYtOWRmNWE5ODk0NzMxXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg",
                     trailerUrl = "https://www.youtube.com/watch?v=kYzz0FSgpSU",
                     franchiseId = 3
                 });
            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    movieId = 5,
                    movieTitle = "Harry Potter and the chamber of secrets",
                    genre = "Fantasy, Adventure",
                    releaseYear = "2002",
                    director = "Chris Columbus",
                    moviePicture = "https://m.media-amazon.com/images/M/MV5BMTcxODgwMDkxNV5BMl5BanBnXkFtZTYwMDk2MDg3._V1_FMjpg_UX1000_.jpg",
                    trailerUrl = "https://www.youtube.com/watch?v=1bq0qff4iF8",
                });
            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    movieId = 6,
                    movieTitle = "Harry Potter and the half blood prince",
                    genre = "Fantasy, Adventure",
                    releaseYear = "2009",
                    director = "David Yates",
                    moviePicture = "https://m.media-amazon.com/images/M/MV5BNzU3NDg4NTAyNV5BMl5BanBnXkFtZTcwOTg2ODg1Mg@@._V1_.jpg",
                    trailerUrl = "https://www.youtube.com/watch?v=JYLdTuL9Wjw",
                });
            //Seed data for Characters
            modelBuilder.Entity<Character>().HasData(new Character()
            {
                characterId = 1,
                characterName = "Neo",
                alias = "Mr.Anderson",
                gender = "male",
                characterPicture = "https://upload.wikimedia.org/wikipedia/en/c/c6/NeoTheMatrix.jpg",
                });
            modelBuilder.Entity<Character>().HasData(new Character()
            {
                characterId = 2,
                characterName = "Tony Stark",
                alias = "Iron Man",
                gender = "male",
                characterPicture = "https://upload.wikimedia.org/wikipedia/en/f/f2/Robert_Downey_Jr._as_Tony_Stark_in_Avengers_Infinity_War.jpg",
            });
            modelBuilder.Entity<Character>().HasData(new Character()
            {
                characterId = 3,
                characterName = "Pepper Potts",
                gender = "female",
                characterPicture = "https://static.wikia.nocookie.net/disney/images/5/56/Pepper_Potts.jpg/revision/latest?cb=20181223155649",
            });
            modelBuilder.Entity<Character>().HasData(new Character()
            {
                characterId = 4,
                characterName = "Steve Rogers",
                alias = "Captain America",
                gender = "male",
                characterPicture = "https://upload.wikimedia.org/wikipedia/en/6/6b/Chris_Evans_as_Steve_Rogers_Captain_America.jpg",
            });
            modelBuilder.Entity<Character>().HasData(new Character()
            {
                characterId = 5,
                characterName = "Natasha Romanoff",
                alias = "Black Widow",
                gender = "female",
                characterPicture = "https://upload.wikimedia.org/wikipedia/en/f/f6/Scarlett_Johansson_as_Black_Widow.jpg",
            });

            //Defining the relationship between movie and franchise
            //Specifically that it is not required and to not cascade on delete
            modelBuilder.Entity<Movie>()
               .HasOne(m => m.franchise)
               .WithMany(f => f.movies)
               .IsRequired(false)
               .OnDelete(DeleteBehavior.SetNull);
            //Defining the relationship between character and movies
            // We override the automatic names with more fitting names
            //And we also add seed data for the relationship between certain characters and movies
            modelBuilder.Entity<Character>()
                .HasMany(c => c.movies)
                .WithMany(m => m.characters)
                .UsingEntity<Dictionary<string, object>>(
                "Starring",
                b => b.HasOne<Movie>().WithMany().HasForeignKey("movieId"),
                b => b.HasOne<Character>().WithMany().HasForeignKey("characterId"),
                table =>
                {
                    table.HasKey("movieId", "characterId");
                    table.HasData(
                        new { movieId = 2, characterId = 2},
                        new { movieId = 2, characterId = 3},
                        new { movieId = 3, characterId = 1},
                        new { movieId = 4, characterId = 1}
                        );
                }
                );
        }
    }
}
