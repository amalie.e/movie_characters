﻿using AutoMapper;
using MovieCharacters.DTOs.FranchiseDtos;
using MovieCharacters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.Profiles
{
    public class FranchiseProfile: Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>();
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();
        }

    }
}
