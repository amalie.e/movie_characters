# Assignment 3 Movie Characters
API endpoints using EFCore

## Contributors

Amalie Espeseth - https://gitlab.com/amalie.e

Jonas Svåsand - https://gitlab.com/jsva

## Overview
This is a API for retrieving and storing information about movies. This information includes information about the movies themselves, as well as the characters within them and the movie-franchises these movies belong to. You can both retrieve information, update information, delete information, and post new information as well as updating the relationships between movies, characters and franchises.

## Instructions:
1. Clone the repository to your computer
2. Open the project in Visual Studio
3. Edit your connection string in appsettings.json, to connect to your own instance of Microsoft SQL server
4. In the nuget-package manager console run `update-database`
5. If step 4 fails, delete all migrations in migration folder. run `add-migration` in the nuget-package manager console, then repeat step 4
6. Run the solution in Visual Studio, this will bring up our swagger documentation
7. You can test the API calls with swagger.

We have added some characters not connected to movies, and some movies not connected to franchises for you to try to update character in movies and movies in franchise.

## Project status
We interpreted Appendix B Point 5 such that services were optional, and have therefore opted to not use them, and use our repositories directly in the controllers.


